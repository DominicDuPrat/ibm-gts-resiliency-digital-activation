<?php
	include("../../bootstrap.php");


	$result = $mysqli->query("SELECT * FROM leaderboard ORDER by time ASC LIMIT 10");	

	$i = 0; 
	if($result){
     // Cycle through results
    	while ($row = $result->fetch_array()){        	
    		$arr[$i]['Name'] = $row["name"];	
			$arr[$i]['Time'] = $row["time"];
			$arr[$i]['Cost'] = $row["cost"];
			$arr[$i]['Reputation'] = $row["reputation"];	
    		$i = $i + 1;
    	}
    	// Free result set
    	$result->close();
	}
	$mysqli->close();
	echo json_encode($arr);	
?>