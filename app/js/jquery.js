// Name variable
var name
var commandChoices = [];
var jobDepartment = [
    "IT Operations",
    "Business Operations",
    "Security & Risk",
    "Customer Relations",
    "Compliance & Legal",
    "C-suite & Board"
]
var jobTitles = [
    "Chief Information Officer",
    "CEO & C-Suite",
    "Business Continuity / DR Manager",
    "Chief Information Security Officer",
    "Legal & Compliance Head",
    "External Communication",
    "Call Centre Manager",
    "Customer Support Manager",
    "Head of HR",
    "Data Centre Manager",
    "Head of Sales",
    "Network Systems Administrator"
]

// Clear Stored Command choices from last player

function clearVariables() {
    commandChoices = [];
    $(".gameScoresContainer").remove();

    // set score cookies
    //setCookie('QUAK', 'mRj#S*&wa6x>',7);

    Cookies.set('QUAK', 'mRj#S*&wa6x>',{ expires: 7, path: '',secure: true, sameSite: 'lax' });
}


// Homepage functions 

function homeSlider() {
    setInterval(function(){
        if ($(".slide.active").next().length) {
            $(".slide.active").next().addClass("active");
            $(".slide.active").prev().removeClass("active");
        } else {
            $(".slide").first().addClass("active");
            $(".slide").last().removeClass("active");
        }
    }, 5000);
}

// Animated begin button

function buttonFadeIn() {
    if ($(".buttonAnimated").length) {
        setTimeout(function(){
          $(".buttonAnimated").addClass("active");  
        }, 3000);
    }
}

// Enter name form function

function enterName(){
    
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
            //e.preventDefault();
        }
    });
    
    
    $("form .outline .buttonNext").click(function(e){
        e.preventDefault();
        if ($(".name-input").val().length) {
            name = $(".name-input").val();
            $(".page .gameContainer h1 .name").html(name)
            $(".active").next().addClass("active");
            $(".active").prev().removeClass("active");
        } else {
        }
    });
    $("#app .buttonReturn").click(function(){
        $(".name-input").val("");
        $(".active").prev().addClass("active");
        $(".active").next().removeClass("active");
    });
//    $(".nextPage").click(function(){
//        $(".active").next().addClass("active");
//        $(".active").prev().removeClass("active");
//    });
    
    
}

// Animation for game instructions

function gameInstructions(){
    $(".instructions .instructionContent.active").fadeIn();
    setInterval(function(){
        if ($(".instructions .instructionContent.active").next().length) {
            $(".instructions .instructionContent.active").next().fadeIn().addClass("active");
            $(".instructions .instructionContent.active").prev().fadeOut().removeClass("active");
        } else {
            $(".instructions .instructionContent.active").fadeOut();
            $("#game-explanation .button").removeClass('buttonNotActive');
        }
    }, 2000);
}

// Team Select 

function teamSelect(){
    $("#teamAssemble .member a").click(function(e){
        e.preventDefault();
        if ($("#teamAssemble .member a.active").length < 4 ){
            $(this).toggleClass("active");
        } else if ($("#teamAssemble .member a.active").length < 5 ) {
            $(this).toggleClass("active");
            setTimeout(function(){
               window.location = '/#/level-1-team-results'
            }, 250);
        } else {
            
        }
    });
    
    $(".button").click(function(e){
        
//        $("#teamAssemble .member a.active").each(function(){
//            commandChoices.push($(this).data("choice"))
//        });
        //console.log(commandChoices);
    });
}

// Add team selected

function addTeamSelected(){
    var counter = 0;
    commandChoices.forEach(function(choice){
        counter++
        $("#teamResults .resultsContainer .row").append(
            '<div class="choice">' +
                '<div class="choice-internal">' +
                    '<div class="title">' +
                        '<p>' + jobDepartment[counter] + '</p>' +
                    '</div>' +
                    '<div class="department">' +
                        '<p>' + jobTitles[choice] + '</p>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    });
}

// End of level one add name

function addNameLevel1() {
    $("#app #level2-warRoom .name").html(name);
}

function addName() {
    $("#app #teamAssemble .name").html(name);
}

// Level 2 warning add class to change color of svg

function level2ChangeColor() {
    setInterval(function(){
        if ($("#level-2-game-complete .headingAnimate").hasClass("active")) {
            $("#level-2-game-complete .headingAnimate").removeClass("active");
        } else {
            $("#level-2-game-complete .headingAnimate").addClass("active");
        }
    }, 1000);
}


// Game Leve 2

var correctCards = 0;

function gameInit() {

  // Reset the game
  correctCards = 0;
  $('#cardPile').html( '' );
  $('#cardSlots').html( '' );
  var numOfCards=8;
  // Create the pile of shuffled cards
  var numbers = [ 1, 2, 3, 4, 5, 6, 7, 8 ];
  var text = [ 
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="44.22" height="47.33" viewBox="0 0 44.22 47.33"><path class="cls-1" d="M44.7,48.25H36a.67.67,0,0,1-.67-.67V40.74h0a.68.68,0,0,1,.67-.67h2.64a.68.68,0,0,1,.54.27l.45.71,5.07,0a.68.68,0,0,1,.67.67v5.91A.67.67,0,0,1,44.7,48.25Zm-8.65-7.46v6.79l8.65,0-.05-5.85-5.07,0a.66.66,0,0,1-.54-.26l-.45-.71Zm-8.44,7.46h-8.7a.67.67,0,0,1-.67-.67V40.74h0a.67.67,0,0,1,.67-.67h2.64a.68.68,0,0,1,.54.27l.45.71,5.07,0a.68.68,0,0,1,.67.67v5.91A.67.67,0,0,1,27.61,48.25ZM19,40.79v6.79l8.65,0-.05-5.85-5.07,0a.7.7,0,0,1-.54-.26l-.45-.71Zm-8.44,7.46H1.82a.67.67,0,0,1-.67-.67V40.74h0a.67.67,0,0,1,.67-.67H4.46a.68.68,0,0,1,.54.27l.45.71,5.07,0a.68.68,0,0,1,.67.67v5.91A.67.67,0,0,1,10.52,48.25ZM1.87,40.79v6.79l8.65,0,0-5.85-5.08,0a.67.67,0,0,1-.53-.26l-.45-.71Zm41.47,2.8H41.91v-.72h1.43Zm-17.1,0H24.82v-.72h1.42Zm-17.09,0H7.73v-.72H9.15Zm31.56-5H40V31.94H25.93a2.69,2.69,0,0,1-2.31,2.3v4.33H22.9V34.24a2.69,2.69,0,0,1-2.3-2.3H6.53v6.63H5.81v-7a.36.36,0,0,1,.36-.36H20.6a2.69,2.69,0,0,1,2.3-2.31V26.5H12.39a.36.36,0,0,1-.36-.36V13.71a.36.36,0,0,1,.36-.36h.45l0,0A2.66,2.66,0,0,1,12,11.43a2.7,2.7,0,0,1,2.4-2.73,3.1,3.1,0,0,1,0-.43A4.25,4.25,0,0,1,18.6,4a4.37,4.37,0,0,1,1.48.28,5,5,0,0,1,9.45,0,5,5,0,0,1,6.16,2.88,5.19,5.19,0,0,1,.37,1.91.11.11,0,0,1,0,0,5,5,0,0,1-2.43,4.25h2.07a.36.36,0,0,1,.36.36V26.14a.35.35,0,0,1-.1.25.36.36,0,0,1-.26.11H23.62v2.41a2.69,2.69,0,0,1,2.31,2.31H40.35a.36.36,0,0,1,.36.36Zm-17.45-9a2,2,0,1,0,2,2A2,2,0,0,0,23.26,29.6ZM12.75,25.78H35.33v-5.5H12.75Zm0-6.22H35.33V14.07H12.75ZM14.62,9.09V9.4a2,2,0,0,0-1.38.61,2,2,0,0,0-.55,1.41,2,2,0,0,0,.6,1.38,2,2,0,0,0,1.41.55H31A4.31,4.31,0,0,0,35.33,9a.11.11,0,0,1,0,0A4.33,4.33,0,0,0,35,7.41a4.3,4.3,0,0,0-5.59-2.35.37.37,0,0,1-.29,0A.36.36,0,0,1,29,4.82a4.28,4.28,0,0,0-8.29.06.33.33,0,0,1-.2.24.34.34,0,0,1-.31,0,3.62,3.62,0,0,0-1.55-.38,3.53,3.53,0,0,0-3.52,3.53,3.35,3.35,0,0,0,.07.71.38.38,0,0,1-.08.31.33.33,0,0,1-.3.12l-.13-.32Zm18,14.3h-3v-.72h3Zm-7.76,0H15.49v-.72h9.33Zm7.76-6.22h-3v-.72h3Zm-7.76,0H15.49v-.72h9.33Z" transform="translate(-1.15 -0.92)"/></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="40.89" height="35.15" viewBox="0 0 40.89 35.15"><g id="data--backup-2"><path class="cls-1" d="M35.3,9.87H6.6a.36.36,0,0,1-.36-.36V.91A.36.36,0,0,1,6.6.55H35.3a.36.36,0,0,1,.36.36v8.6A.36.36,0,0,1,35.3,9.87ZM7,9.15h28V1.27H7Zm7.63-3.58H9.88V4.85h4.71Z" transform="translate(-0.5 -0.55)"/><path class="cls-1" d="M35.3,22.79H6.6a.36.36,0,0,1-.36-.36V13.82a.36.36,0,0,1,.36-.36H35.3a.36.36,0,0,1,.36.36v8.61A.36.36,0,0,1,35.3,22.79ZM7,22.07h28V14.18H7Zm7.63-3.59H9.88v-.72h4.71Z" transform="translate(-0.5 -0.55)"/><path class="cls-1" d="M35.3,35.7H6.6a.36.36,0,0,1-.36-.36V26.73a.36.36,0,0,1,.36-.36H35.3a.36.36,0,0,1,.36.36v8.61A.36.36,0,0,1,35.3,35.7ZM7,35h28V27.09H7Zm7.63-3.59H9.88v-.72h4.71Z" transform="translate(-0.5 -0.55)"/><path class="cls-1" d="M6.6,31.4H.86A.36.36,0,0,1,.5,31V18.12a.36.36,0,0,1,.36-.36H6.6v.72H1.22v12.2H6.6ZM41,18.48H35.3v-.72h5.38V5.57H35.3V4.85H41a.36.36,0,0,1,.36.36V18.12A.36.36,0,0,1,41,18.48Z" transform="translate(-0.5 -0.55)"/></g></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="47.33" height="47.34" viewBox="0 0 47.33 47.34"><path class="cls-1" d="M36.67,47.94H14.92a.36.36,0,0,1-.36-.36V39.81a.36.36,0,0,1,.36-.36H31.65V35.93a2.75,2.75,0,0,0-2.74-2.75H22.69a3.47,3.47,0,0,1-3.47-3.46V26.19h-4.3a.36.36,0,0,1-.36-.36V18.06a.36.36,0,0,1,.36-.36H36.67a.36.36,0,0,1,.36.36v7.77a.36.36,0,0,1-.36.36H19.94v3.52a2.75,2.75,0,0,0,2.75,2.75H28.9a3.47,3.47,0,0,1,3.47,3.47v3.52h4.3a.36.36,0,0,1,.36.36v7.77A.36.36,0,0,1,36.67,47.94Zm-21.39-.72h21v-7h-21Zm0-21.75h21v-7h-21Zm5.86,18.59H18v-.72h3.11ZM39.78,29.3v-.72c4.5,0,7.41-2.39,7.41-6.09a7.12,7.12,0,0,0-6.83-7.18A.35.35,0,0,1,40,15,13.67,13.67,0,0,0,26.34,1.33H26c-4.87,0-9.72,3-12.36,7.74a.34.34,0,0,1-.37.18,10.6,10.6,0,0,0-1.78-.15C5.7,9.1,1.29,13.31,1.29,18.9s4.53,9.68,10.52,9.68v.72C5.41,29.3.57,24.83.57,18.9s4.72-10.52,11-10.52a11.35,11.35,0,0,1,1.65.12C16,3.7,21,.61,26,.61h.3a14.4,14.4,0,0,1,14.39,14,7.81,7.81,0,0,1,7.18,7.87C47.91,26.62,44.72,29.3,39.78,29.3Zm-18.64-7H18v-.72h3.11Z" transform="translate(-0.57 -0.61)"/></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="40.89" height="35.15" viewBox="0 0 40.89 35.15"><g id="data--backup"><path class="cls-1" d="M35.51,10H6.81a.36.36,0,0,1-.36-.36V1A.36.36,0,0,1,6.81.63h28.7a.36.36,0,0,1,.36.36V9.6A.36.36,0,0,1,35.51,10ZM7.17,9.24h28V1.35h-28ZM32.35,5.65H27.64V4.93h4.71Z" transform="translate(-0.71 -0.63)"/><path class="cls-1" d="M35.51,22.87H6.81a.36.36,0,0,1-.36-.36V13.9a.36.36,0,0,1,.36-.36h28.7a.36.36,0,0,1,.36.36v8.61A.36.36,0,0,1,35.51,22.87ZM7.17,22.15h28V14.26h-28Zm7.63-3.59H10.09v-.72H14.8Z" transform="translate(-0.71 -0.63)"/><path class="cls-1" d="M35.51,35.78H6.81a.36.36,0,0,1-.36-.36v-8.6a.36.36,0,0,1,.36-.36h28.7a.36.36,0,0,1,.36.36v8.6A.36.36,0,0,1,35.51,35.78ZM7.17,35.06h28V27.18h-28Zm25.18-3.59H27.64v-.71h4.71Z" transform="translate(-0.71 -0.63)"/><path class="cls-1" d="M6.81,31.48H1.07a.36.36,0,0,1-.36-.36V18.21a.36.36,0,0,1,.36-.36H6.81v.72H1.43V30.76H6.81ZM41.25,18.57H35.51v-.72h5.38V5.65H35.51V4.93h5.74a.36.36,0,0,1,.35.36V18.21A.36.36,0,0,1,41.25,18.57Z" transform="translate(-0.71 -0.63)"/></g></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="47.34" height="47.33" viewBox="0 0 47.34 47.33"><g id="locked--network--alt"><path class="cls-1" d="M44.52,47.78a3.47,3.47,0,0,1-.36-6.92V35.35H24.68V41.2H24V35.35H4.48V41.2H3.76V35a.36.36,0,0,1,.36-.36H24V28.77h.72v5.86H44.52a.36.36,0,0,1,.36.36v5.87a3.47,3.47,0,0,1-.36,6.92Zm0-6.58v.36a2.75,2.75,0,1,0,2.75,2.75,2.75,2.75,0,0,0-2.75-2.75Z" transform="translate(-0.65 -0.44)"/><path class="cls-1" d="M24.32,47.78a3.47,3.47,0,1,1,3.46-3.47A3.47,3.47,0,0,1,24.32,47.78Zm0-6.22a2.75,2.75,0,1,0,2.74,2.75A2.75,2.75,0,0,0,24.32,41.56Z" transform="translate(-0.65 -0.44)"/><path class="cls-1" d="M4.12,47.78a3.47,3.47,0,1,1,3.46-3.47A3.48,3.48,0,0,1,4.12,47.78Zm0-6.22a2.75,2.75,0,1,0,2.75,2.75A2.75,2.75,0,0,0,4.12,41.56Z" transform="translate(-0.65 -0.44)"/><path class="cls-1" d="M24.31,19.81a.36.36,0,0,1-.35-.36V16.34a.36.36,0,1,1,.71,0v3.11A.36.36,0,0,1,24.31,19.81Z" transform="translate(-0.65 -0.44)"/><circle class="cls-2" id="Ellipse_13" data-name="Ellipse 13" class="cls-1" cx="23.67" cy="15.9" r="1.55"/><path class="cls-1" d="M32.08,26H16.55a.36.36,0,0,1-.36-.36V10.12a.36.36,0,0,1,.36-.36h3.52V4.69a4.25,4.25,0,1,1,8.49,0V9.76h3.52a.36.36,0,0,1,.36.36V25.66A.36.36,0,0,1,32.08,26ZM16.91,25.3H31.73V10.48H16.91ZM20.79,9.76h7.05V4.69a3.53,3.53,0,1,0-7,0Z" transform="translate(-0.65 -0.44)"/></g></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="47.33" height="47.34" viewBox="0 0 47.33 47.34"><path class="cls-1" d="M33.64,47.78H11.89a.36.36,0,0,1-.36-.36V39.65a.36.36,0,0,1,.36-.36h4.3V35.76a3.46,3.46,0,0,1,3.46-3.46h6.22a2.75,2.75,0,0,0,2.75-2.75V26H11.89a.36.36,0,0,1-.36-.36V17.9a.36.36,0,0,1,.36-.36H33.64a.36.36,0,0,1,.36.36v7.76a.36.36,0,0,1-.36.36h-4.3v3.53A3.48,3.48,0,0,1,25.87,33H19.66a2.75,2.75,0,0,0-2.75,2.74v3.53H33.64a.36.36,0,0,1,.36.36v7.77A.36.36,0,0,1,33.64,47.78Zm-21.39-.72h21V40h-21Zm0-21.76h21v-7h-21ZM30.53,43.89H27.42v-.72h3.11Zm6.22-14.76v-.72c6,0,10.51-4.16,10.51-9.68S42.86,8.93,37,8.93a10.5,10.5,0,0,0-1.77.16.36.36,0,0,1-.37-.18C32.24,4.2,27.39,1.16,22.52,1.16h-.25A13.67,13.67,0,0,0,8.54,14.79a.36.36,0,0,1-.34.36,7.1,7.1,0,0,0-6.83,7.17c0,3.7,2.91,6.09,7.41,6.09v.72c-4.94,0-8.13-2.67-8.13-6.81a7.81,7.81,0,0,1,7.18-7.87A14.4,14.4,0,0,1,22.22.44h.31c5,0,10.06,3.09,12.85,7.9A11.22,11.22,0,0,1,37,8.21c6.25,0,11,4.53,11,10.52S43.15,29.13,36.75,29.13Zm-6.22-7H27.42v-.72h3.11Z" transform="translate(-0.65 -0.44)"/></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="44" height="37" viewBox="0 0 44.22 34.9"><path class="cls-1" d="M44.39,35.68h-8.7A.67.67,0,0,1,35,35V28.18h0a.68.68,0,0,1,.67-.67h2.64a.68.68,0,0,1,.54.27l.45.71,5.07,0a.67.67,0,0,1,.67.67V35A.67.67,0,0,1,44.39,35.68Zm-8.65-7.45V35l8.65,0,0-5.85-5.07.05a.7.7,0,0,1-.54-.26l-.45-.71ZM27.3,35.68H18.6a.67.67,0,0,1-.67-.67V28.18h0a.67.67,0,0,1,.67-.67h2.64a.68.68,0,0,1,.54.27l.45.71,5.07,0a.67.67,0,0,1,.67.67V35A.67.67,0,0,1,27.3,35.68Zm-8.65-7.45V35l8.65,0-.05-5.85-5.07.05a.7.7,0,0,1-.54-.26l-.45-.71Zm-8.44,7.45H1.51A.67.67,0,0,1,.84,35V28.18h0a.67.67,0,0,1,.67-.67H4.15a.68.68,0,0,1,.54.27l.45.71,5.07,0a.67.67,0,0,1,.67.67V35A.67.67,0,0,1,10.21,35.68ZM1.56,28.23V35l8.65,0-.05-5.85-5.08.05a.67.67,0,0,1-.53-.26l-.45-.71ZM43,31H41.6v-.72H43Zm-17.1,0H24.51v-.72h1.42ZM8.84,31H7.42v-.72H8.84ZM40.4,26h-.72V19.37H25.62a2.69,2.69,0,0,1-2.31,2.31V26h-.72V21.68a2.69,2.69,0,0,1-2.3-2.31H6.22V26H5.5V19a.36.36,0,0,1,.36-.36H20.29a2.69,2.69,0,0,1,2.3-2.3V13.93H12.08a.36.36,0,0,1-.36-.36V1.15a.36.36,0,0,1,.36-.36h23.3a.36.36,0,0,1,.36.36V13.57a.34.34,0,0,1-.36.36H23.31v2.42a2.69,2.69,0,0,1,2.31,2.3H40a.36.36,0,0,1,.36.36ZM23,17a2,2,0,1,0,2,2A2,2,0,0,0,23,17ZM12.44,13.21H35V7.72H12.44Zm0-6.21H35V1.51H12.44Zm19.83,3.83h-3v-.72h3Zm-7.76,0H15.18v-.72h9.33Zm7.76-6.22h-3V3.89h3Zm-7.76,0H15.18V3.89h9.33Z" transform="translate(-0.84 -0.79)"/></svg>',
      '<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="47.34" height="47.34" viewBox="0 0 47.34 47.34"><g id="locked--network"><path class="cls-1" d="M44.43,48a3.47,3.47,0,0,1-.36-6.92V35.59H24.59v5.86h-.72V35.59H4.39v5.86H3.67V35.23A.37.37,0,0,1,4,34.87H23.87V29h.72v5.85H44.43a.36.36,0,0,1,.36.36v5.88A3.47,3.47,0,0,1,44.43,48Zm0-6.58v.36a2.75,2.75,0,1,0,2.75,2.75,2.75,2.75,0,0,0-2.75-2.75ZM24.23,26.27A8.12,8.12,0,0,1,18.48,12.4a8.72,8.72,0,0,1,.73-.65v-6a5,5,0,0,1,10,0v6.05a8.11,8.11,0,0,1,3.11,6.38h0a8.13,8.13,0,0,1-8.13,8.13Zm0-15.54a7.41,7.41,0,1,0,0,14.82h0a7.41,7.41,0,1,0,0-14.82Zm0-.72a8.07,8.07,0,0,1,4.3,1.24V5.71a4.3,4.3,0,0,0-8.6,0v5.53A8.09,8.09,0,0,1,24.23,10Z" transform="translate(-0.57 -0.69)"/><path class="cls-1" d="M24.23,48a3.47,3.47,0,1,1,3.47-3.46A3.47,3.47,0,0,1,24.23,48Zm0-6.21A2.75,2.75,0,1,0,27,44.56,2.75,2.75,0,0,0,24.23,41.81Z" transform="translate(-0.57 -0.69)"/><path class="cls-1" d="M4,48A3.47,3.47,0,1,1,7.5,44.56,3.46,3.46,0,0,1,4,48Zm0-6.21a2.75,2.75,0,1,0,2.75,2.75A2.75,2.75,0,0,0,4,41.81Z" transform="translate(-0.57 -0.69)"/><path class="cls-1" d="M24.23,20.06a.36.36,0,0,1-.36-.36V16.59a.36.36,0,0,1,.72,0V19.7A.36.36,0,0,1,24.23,20.06Z" transform="translate(-0.57 -0.69)"/><circle class="cls-2" id="Ellipse_16" data-name="Ellipse 16" class="cls-1" cx="23.67" cy="15.9" r="1.55"/></g></svg>'];
    numbers.sort( function() { return Math.random() - .5 } );

  var tabNumber = 0;  
  var numberID = 0;
  for ( var i=0; i<text.length; i++ ) {
    tabNumber++
    $('<div>' + text[numbers[i]-1] + '</div>').data( 'number', numbers[i] ).attr( 'id', 'card'+numbers[i] ).attr('tabindex', tabNumber).appendTo( '#cardPile' ).draggable( {
      containment: '#content',
      stack: '#cardPile div',
      cursor: 'move',
      revert: true

    } );
  }

  // Create the card slots
  var words = [ 'icon-1',  'icon-2', 'icon-3','icon-4', 'icon-5', 'icon-6', 'icon-7', 'icon-8'];
  for ( var i=1; i<=words.length; i++ ) {
    tabNumber++
    numberID++
    $('<div id="' + words[i-1] + '" class="card' + tabNumber + '">' + words[i-1] + '</div>').data( 'number', i ).attr('tabindex', tabNumber).attr('data-class', 'card' + numberID).appendTo( '#cardSlots' ).droppable( {
      accept: '#cardPile div',
      hoverClass: 'hovered',
      drop: handleCardDrop
    } );
  }
    

    
    

}

function handleCardDrop( event, ui ) {
  var slotNumber = $(this).data( 'number' );
  var cardNumber = ui.draggable.data( 'number' );

  // If the card was dropped to the correct slot,
  // change the card colour, position it directly
  // on top of the slot, and prevent it being dragged
  // again

  if ( slotNumber == cardNumber ) {
     if(slotNumber==1){
        $('#message').show().html("");
      }
      if(slotNumber==2){
        $('#message').show().html("");
      }
      if(slotNumber==3){
        $('#message').show().html("");
      }
      if(slotNumber==4){
        $('#message').show().html("");
      }
      if(slotNumber==5){
        $('#message').show().html("");
      }
      if(slotNumber==6){
        $('#message').show().html("");
      }
      if(slotNumber==7){
        $('#message').show().html("");
      }
      if(slotNumber==8){
        $('#message').show().html("");
      }

        ui.draggable.addClass( 'correct' );
        ui.draggable.draggable( 'disable' );
        $(this).droppable( 'disable' );
        ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
        ui.draggable.draggable( 'option', 'revert', false );
        correctCards++;

  }

  // If all the cards have been placed correctly then display a message
  // and reset the cards for another go

  if ( correctCards == 8 ) {
    //$("#level-2-game .button").removeClass("buttonNotActive");
      setTimeout(function(){
          window.location = '/#/level-2-game-complete'
      }, 500);
  }
}

function tabDragGame() {
    //var correctCards = 0;

    $('#cardPile div').blur(function() {
        $('.ui-draggable').removeClass("focus");
    }).focus(function() {
         $(this).addClass("focus");
         $('#cardPile').keypress(function(e){
           if(e.keyCode == 13){
               $('.ui-draggable').removeClass("move");
               $('.focus').addClass("move");
           } else {
               
           }
        });
    });
    $('#cardSlots div').blur(function() {
        $('.ui-droppable').removeClass("focus");
    }).focus(function() {
        $(this).addClass("focus");
        
         $('#cardSlots').keydown(function(e){
             
           if(e.keyCode == 13){
               $('.focus').addClass("move");
               $("#cardSlots .correct").removeClass("move");
               if ($("#cardPile .move").attr("id") === $("#cardSlots .move").data("class")) {
                   //$("#cardPile .move").hide();
                   var coordinatescardPile = $("#cardPile .move").offset();
                   var coordinatescardSlots = $("#cardSlots .move").offset();
                   var newLeftCo = coordinatescardSlots.left - coordinatescardPile.left;
                   var newTopCo = coordinatescardSlots.top - coordinatescardPile.top;
                   $("#cardPile .move").css({"left": newLeftCo, "top": newTopCo});
                   $("#cardPile .move").addClass("correct ui-draggable-disabled");
                   $("#cardPile .ui-draggable-disabled").prop('tabIndex', -1);
                   $("#cardSlots .move").addClass("correct").removeClass("move");
                   $("#cardPile .move").removeClass("move");
               } else {
                   $("#cardSlots div").removeClass("move");
               }
               if ($("#cardSlots .correct").length === 8 ) {
                  setTimeout(function(){
                      window.location = '/#/level-2-game-complete'
                  }, 500);
              }
           } else {
               
           }
        });
    });
    
    
}


// Level 3 js

// Game 3 numbers

function showGoldenCopy() {
    var numberList = [];
    var generateArrayAndRandomize = function generateArrayAndRandomize() {
      numberList = [
            {
                active: 'active',
                number: '01101010'
            },
            {
                active: 'notActive',
                number: '00010111'
            },
            {
                active: 'notActive',
                number: '01010000'
            },
            {
                active: 'notActive',
                number: '01101110'
            },
            {
                active: 'notActive',
                number: '11100110'
            },
            {
                active: 'notActive',
                number: '00011110'
            },
            {
                active: 'notActive',
                number: '00100011'
            },
            {
                active: 'notActive',
                number: '01111100'
            },
            {
                active: 'notActive',
                number: '00011010'
            },
            {
                active: 'notActive',
                number: '00001011'
            },
            {
                active: 'notActive',
                number: '01010110'
            },
            {
                active: 'notActive',
                number: '00000111'
            },
            {
                active: 'notActive',
                number: '00100110'
            },
            {
                active: 'notActive',
                number: '00110010'
            },
            {
                active: 'notActive',
                number: '11101001'
            },
            {
                active: 'notActive',
                number: '10000100'
            },
            {
                active: 'notActive',
                number: '00101000'
            },
            {
                active: 'notActive',
                number: '11000101'
            },
            {
                active: 'notActive',
                number: '01111000'
            },
            {
                active: 'notActive',
                number: '00000100'
            },
            {
                active: 'notActive',
                number: '01110100'
            },
            {
                active: 'notActive',
                number: '00000001'
            },
            {
                active: 'notActive',
                number: '10000110'
            },
            {
                active: 'notActive',
                number: '01011001'
            }
        ];
      numberList.sort(function () {
        return Math.random() - 0.5;
      });
      return numberList;
        
    };
    generateArrayAndRandomize();
    var numberItem = 0;
    numberList.forEach(function(listItem){
        //console.log(listItem)
        numberItem++
        $(".numberList").append("<div class='" + listItem.active + "' tabindex='" + numberItem + "'>" + listItem.number + "</div>");
    });
}

function clickNumbersGame() {
    $('.numberList div').click(function(){
        if ($(this).hasClass("active")) {
            $(this).addClass("pass");
            setTimeout(function(){
              window.location = '/#/level-3-war-room-2'
                
            }, 500);
        } else {
            $(this).addClass("fail");
        }
    });
    $('.numberList div').keydown(function(e){
        if(e.which == 13) {
            if ($(this).hasClass("active")) {
                $(this).addClass("pass");
                setTimeout(function(){
                  window.location = '/#/level-3-war-room-2'

                }, 500);
            } else {
                $(this).addClass("fail");
            }
        }
        
    });
    
//    $("#level-3-game .button").click(function(){
//        //recordTimeScores()
//        clearInterval(gameTime);
//        clearInterval(costTime);
//    });
    
}

// Game timer

var timer

function gameTimer() {
    var time = 0;
    timer = setInterval(function(){
        time++;
        //console.log(time);
        if (time == 50) {
            //window.location.href = "/#/game-over";
            if ($(".helpText").length) {
                $(".helpText").fadeIn();
                helpButton();
            }
            clearInterval(timer);
        } 
    }, 1000);
    
//    $("#app .button, #app .buttonHelp, #app .buttonHelp2").click(function(){
//        clearInterval(timer);
//    });
    $("#app .buttonHelp").click(function(){
        clearInterval(timer);
    });
    $("#app .buttonHelp2").click(function(){
        clearInterval(timer);
    });
}


// Total timer

var gameTime
var costTime
var reputation
var seconds
var minutes

function mainTimerAdd() {
    if ($(".gameScores").length) {
        
    } else {
        $(".siteContainer").prepend('<div class="container"><div class="row"><div class="nine columns offset-by-six"><div class="gameScoresContainer"><div class="gameScores"><div class="score score-1"><div class="score-time"><span class="hours">00</span>:<span class="minutes">00</span>:<span class="seconds">00</span></div><div class="score-title">TIME</div></div><div class="score score-2"><div class="score-time">$<span><span class="cost">0</span></span></div><div class="score-title">COST</div></div><div class="score score-3"><div class="score-time"><img src="dist/img/Emojis-01.svg" class="scale-with-grid" alt="Emoji" /></div><div class="score-title">REPUTATION</div></div></div></div></div></div></div>');
    }
}

// Add comma to updaing number
function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

var cost,
    costString,
    rep

var emoji = [
    "dist/img/Emojis-01.svg",
    "dist/img/Emojis-02.svg",
    "dist/img/Emojis-03.svg",
    "dist/img/Emojis-04.svg",
    "dist/img/Emojis-05.svg",
    "dist/img/Emojis-06.svg",
    "dist/img/Emojis-07.svg",
    "dist/img/Emojis-08.svg",
    "dist/img/Emojis-09.svg",
    "dist/img/Emojis-10.svg",
    "dist/img/Emojis-11.svg",
    "dist/img/Emojis-12.svg",
    "dist/img/Emojis-13.svg",
];

var emojiCounter;

function mainTimer() {
    clearInterval(gameTime);
    clearInterval(costTime);
    $(".siteContainer .gameScores .score-1 .score-time span").html("00");
    seconds = 0;
    minutes = 00;
    hours = 0;
    emojiCounter = 0;
    gameTime = setInterval(function(){
        
        if (seconds < 9) {
            seconds++
            $(".gameScoresContainer .gameScores .seconds").html("0" + seconds);
        } else if (seconds > 9 && seconds >= 59) {
            seconds = 0;
            minutes++;
            $(".gameScoresContainer .gameScores .seconds").html("00");
            if (minutes < 10) {
               $(".gameScoresContainer .gameScores .minutes").html("0" + minutes); 
            } else {
                if (minutes > 59) {
                    hours++;  
                    minutes = 0;
                    $(".gameScoresContainer .gameScores .minutes").html("0" + minutes); 
                    if (hours < 10) {
                        $(".gameScoresContainer .gameScores .hours").html("0" + hours);""
                    } else {
                        $(".gameScoresContainer .gameScores .hours").html(hours);
                    }
                } else {
                    $(".gameScoresContainer .gameScores .minutes").html(minutes); 
                }
            }
        } else {
            seconds++
            $(".gameScoresContainer .gameScores .seconds").html(seconds);
            // Emoji counter 
            if (seconds == 29 || seconds == 59) {
                emojiCounter++;
                if (emojiCounter > emoji.length) {
                    
                } else {
                   $(".siteContainer .gameScores .score-3 img").attr("src", emoji[emojiCounter]); 
                }
            } 
        }
        
        
        
    }, 1000);
    
    cost = 0;
    costString = 0;
    rep = 0;
    
    costTime = setInterval(function(){
        cost = cost + 10051;
        var num = cost;
        var commaNum = numberWithCommas(num);
        var num = $(".gameScoresContainer .gameScores .score-2 .cost").html(commaNum);
    }, 1000);
    
//    reputation = 0;
//    reputation = setInterval(function(){
//        rep--;
//        $(".gameScoresContainer .gameScores .score-3 .scoreRep").html(rep);
//    }, 6000);
    
//    setTimeout(function(){
//        $(".gameScoresContainer .gameScores .score-3 .score-time").addClass("error");
//    }, 6000);
    
}

// Help button if game isn't finished before the timer runs out.

function helpButton() {
    $(".buttonHelp").click(function(){
        clearInterval(gameTime);
        clearInterval(costTime);
        //clearInterval(reputation);
        seconds = parseInt($(".siteContainer .gameScores .score-1 .score-time .seconds").text()) + 10;
        minutes = parseInt($(".siteContainer .gameScores .score-1 .score-time .minutes").html());
        gameTime = setInterval(function(){
            if (seconds < 9) {
                seconds++
                $(".gameScoresContainer .gameScores .seconds").html("0" + seconds);
            } else if (seconds > 9 && seconds >= 59) {
                seconds = 0;
                minutes++;
                $(".gameScoresContainer .gameScores .seconds").html("00");
                if (minutes < 10) {
                   $(".gameScoresContainer .gameScores .minutes").html("0" + minutes); 
                } else {
                   $(".gameScoresContainer .gameScores .minutes").html(minutes);
                    
                }
            } else {
                seconds++
                $(".gameScoresContainer .gameScores .seconds").html(seconds);
                // Emoji counter 
                if (seconds == 29 || seconds == 59) {
                    emojiCounter++;
                    if (emojiCounter > emoji.length) {

                    } else {
                       $(".siteContainer .gameScores .score-3 img").attr("src", emoji[emojiCounter]); 
                    }
                } 
            }

        }, 1000);

        var cost = parseInt(parseFloat($(".siteContainer .gameScores .score-2 .score-time .cost").html().replace(/,/g, ''))) + 100510;
        console.log(cost);
        var rep = parseInt($(".gameScoresContainer .gameScores .score-3 .scoreRep").html());
        costTime = setInterval(function(){
            cost =  cost + 10051;
            var num = cost;
            var commaNum = numberWithCommas(num);
            var num = $(".gameScoresContainer .gameScores .score-2 .cost").html(commaNum);
        }, 1000);
        
        var rep = parseInt($(".gameScoresContainer .gameScores .score-3 .scoreRep").html() - 10);
        reputation = setInterval(function(){
            rep--;
            $(".gameScoresContainer .gameScores .score-3 .scoreRep").html(rep);
        }, 6000);
    });
    
    $(".buttonHelp2").click(function(){
        clearInterval(gameTime);
        clearInterval(costTime);
        //clearInterval(reputation);
        seconds = parseInt($(".siteContainer .gameScores .score-1 .score-time .seconds").text()) + 10;
        minutes = parseInt($(".siteContainer .gameScores .score-1 .score-time .minutes").html());
        
        if (seconds < 9) {
            $(".gameScoresContainer .gameScores .seconds").html("0" + seconds);
        } else if (seconds > 9 && seconds >= 59) {
            seconds = 0;
            minutes++;
            $(".gameScoresContainer .gameScores .seconds").html("00");
            if (minutes < 10) {
               $(".gameScoresContainer .gameScores .minutes").html("0" + minutes); 
            } else {
               $(".gameScoresContainer .gameScores .minutes").html(minutes); 
            }
        } else {
            $(".gameScoresContainer .gameScores .seconds").html(seconds);
        }

        var cost = parseInt(parseFloat($(".siteContainer .gameScores .score-2 .score-time .cost").html().replace(/,/g, '')));
        var rep = parseInt($(".gameScoresContainer .gameScores .score-3 .scoreRep").html());
        cost =  cost + 100510;
        var num = cost;
        var commaNum = numberWithCommas(num);
        var num = $(".gameScoresContainer .gameScores .score-2 .cost").html(commaNum);
        
//        var rep = parseInt($(".gameScoresContainer .gameScores .score-3 .scoreRep").html() - 10);
//        $(".gameScoresContainer .gameScores .score-3 .scoreRep").html(rep);
    });
    
    
    
}

// Records the finals scores to add to last screen

var timeSpentHours,
    timeSpentMinutes,
    timeSpentSeconds,
    totalTime,
    totalCost,
    totalReputation,
    rank,
    leaderboard

function recordTimeScores() {
    // Stop timers
    clearInterval(gameTime);
    clearInterval(costTime);
    clearInterval(reputation);
    timeSpentHours = $(".siteContainer .gameScores .score .score-time .hours").html();
    timeSpentMinutes = $(".siteContainer .gameScores .score .score-time .minutes").html();
    timeSpentSeconds = $(".siteContainer .gameScores .score .score-time .seconds").html();
    totalTime = timeSpentHours + ":" + timeSpentMinutes + ":" + timeSpentSeconds;
    totalCost = parseInt(parseFloat($(".siteContainer .gameScores .score-2 .score-time .cost").html().replace(/,/g, '')));
    totalReputation = $(".siteContainer .gameScores .score-3 img").attr("src");
    console.log(emojiCounter)
    //console.log(totalReputation)
    
    var dataInput = {};
        dataInput.name = name;
        dataInput.time = totalTime;
        dataInput.cost = totalCost;
        dataInput.reputation = emojiCounter;
        //console.log(dataInput);
    
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var text = "";
	for (var i = 0; i < 12; i++){
	    text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	
    localStorage.setItem('UFS_' + text , JSON.stringify(dataInput));
    
}


function storeScores (){

    var timeSpent = parseInt(timeSpentSeconds) + parseInt(timeSpentMinutes*60) + parseInt(timeSpentHours * 3600);

    $.ajax({
        url: "/php/add.php",
        type: "post",
        dataType: 'json',
        data: { name: name, time: timeSpent, cost: totalCost, reputation: emojiCounter, secret: 'mRj#S*&wa6x>'},
        success: function (response) {
           console.log(response);

            rank = response[0]['Rank'];
          },
          error: function(jqXHR, textStatus, errorThrown) {
             console.log(textStatus, errorThrown);
          }
    });     


}


function setCookie(key,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    } 
    document.cookie = key + "=" + (value || "")  + expires + "; path=/;secure;samesite=strict";
}

//function getCookie(key) {
//    console.log("Set cookie");
//    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
//    console.log(keyValue);
//    return keyValue ? keyValue[2] : null;
//}
//
//function eraseCookie(key) {
//    var keyValue = getCookie(key);
//    setCookie(key, keyValue, '-1');
//}

function addFinalScores() {

    $(".gameScoresContainer").parent().parent().parent().remove();
    $("#final-score .exampleNumber .finalScore p").html(timeSpentHours + ":" + timeSpentMinutes + ":" + timeSpentSeconds);
    $("#final-score .exampleNumber .name").html(name);
    $("#final-score .exampleNumber .reputation img").attr("src", emoji[emojiCounter])
  //  $("#final-score .exampleNumber .cost").html(totalCost)

      var num = totalCost;
      var commaNum = numberWithCommas(num);
      var num = $("#final-score .exampleNumber .cost").html(commaNum);

    $("#final-score .exampleNumber .rank").html(rank)

}


// get data from db
function show_leaderboard() {

    console.log(leaderboard);
    for (i = 0; i < leaderboard.length; i++) {

        var position = i +1; 
       var name= leaderboard[i]['Name'];
       var time= leaderboard[i]['Time'];
       var cost= leaderboard[i]['Cost'];
        var num = leaderboard[i]['Cost'];
        var commaNum = numberWithCommas(num);
       var reputation= leaderboard[i]['Reputation'];

      var minute = Math.floor(time/60);
      var second = time % 60;
      var second = ("0" + second).slice(-2);
                   
           $(".leaderboard").append('<tr><td>'+position+'#</td> <td>'+name+'</td><td>'+minute+':'+second+'</td><td>$'+ commaNum +'</td><td><img src="'+emoji[reputation]+'" class="scale-with-grid" alt="Emojis" /></td></tr>');

    } 



}

// get data from db
function leaderBoard_db() {

    $.ajax({
        url: "/php/leaderboard.php",
        type: "post",
        dataType: 'json',
        success: function (response) {

            leaderboard = response;

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
        
   });   

}   


// Add to leaderboard from local storage

function leaderBoard() {
    var key,
        index;
    var counter = 0;
    $("table").prepend('<tr><th colspan="2">Ranked</th><th>Time</th><th>Cost</th><th>Reputation</th></tr>');
    for (var i = 0; i < localStorage.length; i++){         
        key = localStorage.key(i);
        index = JSON.parse(localStorage.getItem(localStorage.key(i)));
        //console.log(index);
        counter++
        $("table").append('<tr class="score">' +
            '<td width="9%" class="textBlue">' +
                '#' + counter +
            '</td>' +
            '<td width="32%">' +
               index.name +
            '</td>' +
            '<td width="21%" class="textGreen">' +
               index.time +
            '</td>' +
            '<td width="22%" class="textGreen">' +
                '$' + index.cost +
            '</td>' +
            '<td width="16%" class="textGreen">' +
               + index.reputation +
            '</td>' +
        '</tr>' );
    }
    
}

// Auto play videos

function playVideo() {
    $(".playVideo").click(function(e){
        e.preventDefault();
        $(this).fadeOut();
        $(".playMe")[0].play();
    });
    //$(".playMe")[0].play();
    //$(".playVideo").trigger("click");

    //$(".playMe").prop('muted', false); 
}

function playAudio() {
    var x = document.getElementById("alarm");
    x.play();
}

function puaseVideo() {
    var x = document.getElementById("alarm");
    x.pause();
}

// Stop buttons being clickable until the game is complete.

function nonActiveButton() {
    $(".button").click(function(e){
        if($(".button").hasClass("buttonNotActive")) {
            console.log("has class");
            e.preventDefault();
        } else {
            window.location = $(this).attr('href');
        }
    });
}



$(document).ready(function(){
    
    // Disable back button  
    $(window).on('pushstate', function(event) {
     alert("push");
    });

});