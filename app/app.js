(function($) {

//function routing() {

  var app = $.sammy('#app', function() {
    //this.use('Template');

	// Home page
    this.get('/#/', function(context) {		
      context.app.swap('');
       context.render('/templates/home.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           clearVariables();
//           //leaderBoard()
//        }, 200);
    });
    
      
    // Start game 
    this.get('/#/start', function(context) {		
       context.app.swap('');
       context.render('/templates/start.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           clearVariables(); 
//        }, 200);
    });
    
      
    // Invasion page
    this.get('/#/invasion', function(context) {		
        context.app.swap('');
        context.render('/templates/invasion.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
            
    });
    
    // Help needed
    this.get('/#/help-needed', function(context) {		
        context.app.swap('');
        context.render('/templates/help-needed.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);   
    });
      
    // Enter name
    this.get('/#/enter-name', function(context) {		
        context.app.swap('');
        context.render('/templates/name-entry.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            enterName()
//        },200); 
    });
      
    // Game explain
    this.get('/#/game-explain', function(context) {		
        context.app.swap('');
        context.render('/templates/game-explaination.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            gameInstructions()
//            mainTimer()
//        },200); 
    });
      
    // Level 1
//    this.get('/#/level-1-intro', function(context) {		
//        context.app.swap('');
//        context.render('/templates/level-1-intro.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
//    });
    this.get('/#/level-1-team-assemble', function(context) {		
        context.app.swap('');
        context.render('/templates/level-1-team-assemble.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           addName()
//           teamSelect(); 
//        },200);
    });
      
    this.get('/#/level-1-team-results', function(context) {		
        context.app.swap('');
        context.render('/templates/level-1-team-results.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           addTeamSelected() 
//           buttonFadeIn() 
//        },100);
    });
      
    this.get('/#/level-1-game-explain', function(context) {		
        context.app.swap('');
        context.render('/templates/level-1-game-explain.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
    });
    this.get('/#/level-1-game', function(context) {		
        context.app.swap('');
        context.render('/templates/level-1-game.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            grid.init(3);
//            gameTimer();
//        },200);
    });
    this.get('/#/level-1-game-complete', function(context) {		
        context.app.swap('');
        context.render('/templates/level-1-game-complete.template',{}).appendTo(context.$element());
//        $(".gameScoresContainer").css({"opacity": "0"});
//        setTimeout(function(){
//           addNameLevel1()
//        },200);
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
    });
      
    // Level 2
//      
//    this.get('/#/level-2-intro', function(context) {		
//        context.app.swap('');
//        context.render('/templates/level-2-intro.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
//    });
    this.get('/#/level-2-war-room', function(context) {		
        context.app.swap('');
        context.render('/templates/level-2-war-room.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
    });
//    this.get('/#/level-2-runbook', function(context) {		
//        context.app.swap('');
//        context.render('/templates/level-2-runbook.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
//    });
    this.get('/#/level-2-war-room-2', function(context) {		
        context.app.swap('');
        context.render('/templates/level-2-war-room-2.template',{}).appendTo(context.$element());
    });
    this.get('/#/level-2-title-screen', function(context) {		
        context.app.swap('');
        context.render('/templates/level-2-title-screen.template',{}).appendTo(context.$element());
    });
    this.get('/#/level-2-game', function(context) {		
        context.app.swap('');
        context.render('/templates/level-2-game.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            gameInit()
//            buttonFadeIn() 
//            gameTimer()
//        },200);
    });
    this.get('/#/level-2-game-complete', function(context) {		
        context.app.swap('');
        context.render('/templates/level-2-game-complete.template',{}).appendTo(context.$element());
//        level2ChangeColor();
//        $(".gameScoresContainer").css({"opacity": "0"});
//        setTimeout(function(){
//            
//           buttonFadeIn() 
//        },200);
    });
    this.get('/#/level-3-intro', function(context) {		
        context.app.swap('');
        context.render('/templates/level-3-intro.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
    });
    this.get('/#/level-3-war-room', function(context) {		
        context.app.swap('');
        context.render('/templates/level-3-war-room.template',{}).appendTo(context.$element());
        //$(".gameScoresContainer").css({"opacity": "1"});
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
    }); 
    this.get('/#/level-3-game', function(context) {		
        context.app.swap('');
        context.render('/templates/level-3-game.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            showGoldenCopy();
//            clickNumbersGame();
//            gameTimer()
//        }, 200);
    });
    this.get('/#/level-3-game-complete', function(context) {		
        context.app.swap('');
        context.render('/templates/level-3-game-complete.template',{}).appendTo(context.$element());
//        $(".gameScoresContainer").css({"opacity": "0"});
//        setTimeout(function(){
//           buttonFadeIn() 
//           recordTimeScores()
//        },200);
    });
    this.get('/#/level-3-war-room-2', function(context) {		
        context.app.swap('');
        context.render('/templates/level-3-war-room-2.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           buttonFadeIn() 
//        },200);
    }); 
    
    this.get('/#/final-score', function(context) {		
        context.app.swap('');
        context.render('/templates/final-score.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//           addFinalScores()
//           buttonFadeIn() 
//        },200);
    });  
      
    this.get('/#/leaderboard', function(context) {		
        context.app.swap('');
        context.render('/templates/leaderboard.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            leaderBoard()
//        }, 200);
    }); 
    this.get('/#/game-over', function(context) {		
        context.app.swap('');
        context.render('/templates/game-over.template',{}).appendTo(context.$element());
//        setTimeout(function(){
//            $(".siteContainer .gameScoresContainer").remove();
//        }, 200);
    }); 
    
  });

  $(function() {
	  
	  app.run('/#/');
	
  });

//}
})(jQuery);














